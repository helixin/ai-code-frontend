const zyg = 'http://47.112.245.144:'; //张阳光

/**
 * 配置代理
 * @type {[null,null]}
 */
const PROXY_CONFIG = [
  {
    context: [
      '/project',//项目管理控制器
      '/login',//登录注册
      '/framework',//框架技术控制器
      '/displayAttribute',//数据/属性展示方式
      "/relationship", //数据关系
      "/displayAttribute", //字段属性
      "/logs",
      "/res",
    ],
    target: zyg + "8080",   //拦截 context配置路径，经过此地址
    secure: false
  },
];

module.exports = PROXY_CONFIG;
