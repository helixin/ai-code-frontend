import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectDatabaseComponent } from './project-database.component';

describe('ProjectDatabaseComponent', () => {
  let component: ProjectDatabaseComponent;
  let fixture: ComponentFixture<ProjectDatabaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectDatabaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectDatabaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
