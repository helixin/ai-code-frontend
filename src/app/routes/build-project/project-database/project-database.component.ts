import {Component, OnInit} from '@angular/core';
import {ProjectStepsComponent} from '../project-steps/project-steps.component';
import {Setting} from '../../../public/setting/setting';
import {BuildProjectService} from '../build-project.service';
import {ActivatedRoute} from '@angular/router';
import {SetAssociationTypes} from '../../../public/util/enums';
import * as $ from 'jquery';

@Component({
  selector: 'app-project-database',
  templateUrl: './project-database.component.html',
  styleUrls: ['./project-database.component.scss']
})
export class ProjectDatabaseComponent implements OnInit {
  guideLang: any = Setting.PAGEMSG;           //引导语
  dataTables: Array<any> = [];//数据库表
  optionals: Array<any> = [];//操作项
  displayType: Array<any> = [];//操作项
  type: string;
  routerProjectCode: string;
  curTableCode: string;//当前操作表的编码
  curTableName: string;//当前操作表的名
  setAssociationTypes = SetAssociationTypes;
  setType: string;
  buildProInfo:any = {};

  constructor(public steps: ProjectStepsComponent, private routeInfo: ActivatedRoute,
              private buildProjectService: BuildProjectService) {
    this.steps.current = 2;//添加项目的进度条
  }

  ngOnInit() {
    this.type = this.routeInfo.snapshot.queryParams['type'];
    this.routerProjectCode = this.routeInfo.snapshot.queryParams['projectCode'] || sessionStorage.getItem('projectCode');
    if (this.routerProjectCode) sessionStorage.setItem('projectCode', this.routerProjectCode);
    this.getTableList();//查询该项目的所有表
    this.spectPreStep();
  }

  /**
   * 检查上一步是否填写，如果没有跳回到上一步
   */
  spectPreStep() {
    let me = this;
    if (me.routerProjectCode) {
      sessionStorage.setItem('projectCode', me.routerProjectCode);
    }
    let data = {
      code: me.routerProjectCode || sessionStorage.getItem('projectCode')
    };
    $.when(me.buildProjectService.loadProject(data)).done(result => {
      me.buildProInfo = result;
    });
  }

  setTable(code, name, type) {
    this.curTableCode = code;
    this.curTableName = name;
    this.setType = type;
  }

  /**
   * 查询该项目的所有表
   * @param event
   * @param curPage
   */
  public getTableList() {
    let me = this;
    let data = {
      projectCode: me.routerProjectCode
    };
    this.buildProjectService.getTableList(data).then((list: Array<any>) => {
      if (list) {
        this.dataTables = list;
      }
    });
  }

  routerSkip(step) {
    let type = this.buildProInfo.projectFramworkList.length > 0 ? 'edit' : 'add';
    this.buildProjectService.routerSkip(step, type);
  }

}
