import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {BuildProjectService} from '../build-project.service';
import {NzNotificationService} from 'ng-zorro-antd';

@Component({
  selector: 'app-project-database-association',
  templateUrl: './project-database-association.component.html',
  styleUrls: ['./project-database-association.component.scss']
})
export class ProjectDatabaseAssociationComponent implements OnInit, OnChanges {
  @Input() projectCode: string;
  @Input() curTableCode: string;
  @Input() mainTableName: string;
  @Output() savedAssociation = new EventEmitter();
  tableList: Array<any> = [];
  relationshipList: Array<any> = [];
  joinTableName: string;
  joinTableData: any;

  drawerVisible = false; //是否显示抽屉
  mainTableFields: Array<any> = [];
  joinTableFields: Array<any> = [];
  mainField: string;
  joinField: string;

  ngOnChanges(changes: SimpleChanges): void {
    if (changes && changes['curTableCode'] && this.curTableCode) {
      this.getAllTableList();
    }
  }

  constructor(private buildProjectService: BuildProjectService, private _notification: NzNotificationService) {
  }

  ngOnInit() {
    // this.getAllTableList();
  }

  /**
   * 查询该项目的所有表
   * @param event
   * @param curPage
   */
  public getAllTableList() {
    let data = {
      projectCode: this.projectCode
    };
    this.buildProjectService.getTableList(data).then((list: Array<any>) => {
      if (list) {
        this.tableList = list;
        this.getTableRelationshipList();
      }
    });
  }

  /**
   * 查询该项目的所有表
   * @param event
   * @param curPage
   */
  public getTableRelationshipList() {
    let data = {
      classTableCode: this.curTableCode
    };
    this.buildProjectService.getTableRelationshipList(data).then((list: Array<any>) => {
      if (list) {
        this.relationshipList = list;
        this.tableList = this.tableList.filter(item => item.code !== this.curTableCode);//去掉当前表格
        this.tableList.map(item => {
          this.relationshipList.map(it => {
            if (item.code === it.associateCode) {
              item.isOneToOne = it.isOneToOne === 'Y';
              item.isOneToMany = it.isOneToMany === 'Y';
              item.relationCode = it.code;
            }
          });
          // this.refreshStatus(item);
        });
      }
    });
  }

  /**
   * 创建表关系
   */
  buildRelationship(data?) {
    if (data) this.joinTableData = data;
    this.getCheckedFiled();
    let requestData = {
      mapClassTableCode: this.curTableCode,
      associateCode: this.joinTableData.code,
      oneToOne: this.joinTableData.isOneToOne ? 'Y' : 'N',
      oneToMany: this.joinTableData.isOneToMany ? 'Y' : 'N',
      mainField: this.mainField,
      joinField: this.joinField
    };
    this.buildProjectService.buildRelationship(requestData).then(res => {
      this.closeDrawer();
    });
  }

  /**
   * 获取选中的字段
   */
  getCheckedFiled() {
    console.log('█ this.mainTableFields ►►►', this.mainTableFields);
    this.mainTableFields.forEach(item => {
      if (item.checkedField) this.mainField = item.field;
    });
    this.joinTableFields.forEach(item => {
      if (item.checkedField) this.joinField = item.field;
    });
    if (!this.mainField) {
      this._notification.warning('缺少数据', '请选择主表关联属性');
      return;
    }
    if (!this.joinField) {
      this._notification.warning('缺少数据', '请选择副表关联属性');
      return;
    }
  }

  /**
   * 删除表关系
   */
  deleteRelationship(data) {
    if (data) this.joinTableData = data;
    if (!data.relationCode) return;
    let requestData = {
      codes: data.relationCode
    };
    this.buildProjectService.deleteRelationship(requestData).then(res => {
      this.getTableRelationshipList();
    });
  }

  /**
   * 显示抽屉
   */
  showDrawer(data) {
    this.drawerVisible = true;
    this.joinTableName = data.tableName;
    this.joinTableData = data;
    let requestData = {
      mapClassTableCode: this.curTableCode,
      associateCode: data.code
    };
    this.buildProjectService.getListMapFieldColumn(requestData).then((res: any) => {
      if (res.mainFields) this.mainTableFields = res.mainFields;
      if (res.associateFields) this.joinTableFields = res.associateFields;
      this.mainTableFields = this.mainTableFields.map(item => {
        item.checkedField = (res.mapRelationship && (item.field === res.mapRelationship.mainField));
        console.log('█ item.checkedFiled ►►►', item.checkedFiled);
        return item;
      });
      this.joinTableFields = this.joinTableFields.map(item => {
        item.checkedField = res.mapRelationship && item.field === res.mapRelationship.joinField;
        return item;
      });
    });
  }

  closeDrawer() {
    this.drawerVisible = false;
    this.mainField = undefined;
    this.joinField = undefined;
    this.getAllTableList();//弹窗关闭后重新获取表关系，防止未添加成功的情况
  }

  /**
   * 选中所有
   * @param value
   */

  /*checkAll(value: boolean, type): void {
    this.tableList.forEach(data => {
      if (type === SetAssociationTypes.oneToOne) {
        data.isOneToOne = value;
      }
      if (type === SetAssociationTypes.oneToMany) {
        data.isOneToMany = value;
      }
      this.changeRelationship(value, data);
    });
  }*/

  /**
   * 修改关系
   * @param data
   */
  changeRelationship(value, data) {
    if (value) this.showDrawer(data);
    else this.deleteRelationship(data);
  }

  /**
   * 更新状态
   * @param data
   */
  /*refreshStatus(data): void {
    const oneToOneAllChecked = this.tableList.every(value => value.isOneToOne === true);
    const oneToOneAllUnChecked = this.tableList.every(value => !value.isOneToOne);
    const oneToManyAllChecked = this.tableList.every(value => value.isOneToMany === true);
    const oneToManyAllUnChecked = this.tableList.every(value => !value.isOneToMany);
    this.oneToOneAllChecked = oneToOneAllChecked;
    this.oneToManyAllChecked = oneToManyAllChecked;
    this.oneToOneIndeterminate = (!oneToOneAllChecked) && (!oneToOneAllUnChecked);
    this.oneToManyIndeterminate = (!oneToManyAllChecked) && (!oneToManyAllUnChecked);
  }*/
}
