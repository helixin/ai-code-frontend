import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectDatabaseAssociationComponent } from './project-database-association.component';

describe('ProjectDatabaseAssociationComponent', () => {
  let component: ProjectDatabaseAssociationComponent;
  let fixture: ComponentFixture<ProjectDatabaseAssociationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectDatabaseAssociationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectDatabaseAssociationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
