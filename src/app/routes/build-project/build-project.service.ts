import {Injectable} from '@angular/core';
import {AjaxService} from '../../public/service/ajax.service';
import {SettingUrl} from '../../public/setting/setting_url';
import {NzMessageService, NzNotificationService} from 'ng-zorro-antd';
import {Router} from '@angular/router';

import * as $ from 'jquery';

@Injectable()
export class BuildProjectService {

  projectCode: string = '';               //存储项目的编码
  constructor(public router: Router,
              private message: NzMessageService,
              private ajaxService:AjaxService,
              public _notification: NzNotificationService) {
  }

  /**
   * 新建项目
   * @param requestDate
   * @param callback
   */
  buildProject(requestDate: any) {
    let me = this, defer = $.Deferred(); //封装异步请求结果
    this.ajaxService.post({
      url: SettingUrl.URL.projectCtrl.build,
      data: requestDate,
      success: (res) => {
        if (res.success) {
          me._notification.success(`成功了`, res.info);
          defer.resolve(res.data);
        } else {
          me._notification.error(`出错了`, res.info);
        }
      },
      error: () => {
        me._notification.error(`出错了`, '失败，请稍后重试');
      }
    });
    return defer.promise();
  }

  /**
   * 修改项目
   * @param requestDate
   * @param callback
   */
  modifyProject(requestDate: any) {
    let me = this, defer = $.Deferred(); //封装异步请求结果
    this.ajaxService.put({
      url: SettingUrl.URL.projectCtrl.modify,
      data: requestDate,
      success: (res) => {
        if (res.success) {
          defer.resolve(res.data);
          me._notification.success(`成功了`, res.info);
        } else {
          me._notification.error(`出错了`, res.info);
        }
      },
      error: () => {
        me._notification.error(`出错了`, '失败，请稍后重试');
      }
    });
    return defer.promise();
  }

  /**
   * 加载项目的信息
   */
  loadProject(requestDate: any) {
    let me = this, defer = $.Deferred(); //封装异步请求结果
    this.ajaxService.get({
      url: SettingUrl.URL.projectCtrl.load,
      data: requestDate,
      success: (res) => {
        if (res.success) {
          defer.resolve(res.data);
        } else {
          me._notification.error(`出错了`, res.info);
        }
      },
      error: () => {
        me._notification.error(`出错了`, '失败，请稍后重试');
      }
    });
    return defer.promise();
  }

  /**
   * 修改sql
   * @param requestDate
   * @param callback
   */
  modifySql(requestDate: any) {
    let me = this, defer = $.Deferred(); //封装异步请求结果
    this.ajaxService.put({
      url: SettingUrl.URL.projectSqlCtrl.modify,
      data: requestDate,
      success: (res) => {
        if (res.success) {
          me._notification.success(`成功了`, res.info);
          defer.resolve(res.data);
        } else {
          me._notification.error(`出错了`, res.info);
          defer.reject(res.data);
        }
      },
      error: () => {
        me._notification.error(`出错了`, '失败，请稍后重试');
      }
    });
    return defer.promise();
  }

  /**
   * 创建项目sql
   * @param requestDate
   * @param callback
   */
  buildProjectSql(requestDate: any) {
    let me = this, defer = $.Deferred(); //封装异步请求结果
    this.ajaxService.post({
      url: SettingUrl.URL.projectSqlCtrl.build,
      data: requestDate,
      success: (res) => {
        if (res.success) {
          defer.resolve(res.data);
        } else {
          me._notification.error(`出错了`, res.info);
        }
      },
      error: () => {
        me._notification.error(`出错了`, '失败，请稍后重试');
      }
    });
    return defer.promise();
  }

  /**
   * 加载项目SQL的信息
   */
  loadSql(requestDate: any) {
    let me = this, defer = $.Deferred(); //封装异步请求结果
    this.ajaxService.get({
      url: SettingUrl.URL.projectSqlCtrl.load,
      data: requestDate,
      success: (res) => {
        if (res.success) {
          defer.resolve(res.data);
        } else {
          me._notification.error(`出错了`, res.info);
        }
      },
      error: () => {
        me._notification.error(`出错了`, '失败，请稍后重试');
      }
    });
    return defer.promise();
  }

  /**
   * 执行脚本
   * @param requestDate
   * @param callback
   */
  projectInit(requestDate: any) {
    let me = this, defer = $.Deferred(); //封装异步请求结果
    this.ajaxService.put({
      url: SettingUrl.URL.projectCtrl.init,
      data: requestDate,
      success: (res) => {
        if (res.success) {
          me._notification.success(`成功了`, res.info);
          defer.resolve(res.data);
        } else {
          me._notification.error(`出错了`, res.info);
        }
      },
      error: () => {
        me._notification.error(`出错了`, '失败，请稍后重试');
      }
    });
    return defer.promise();
  }

  /**
   * 关联技术框架
   * @param requestDate
   * @param callback
   */
  linkFrames(requestDate: any) {
    let me = this, defer = $.Deferred(); //封装异步请求结果
    this.ajaxService.post({
      url: SettingUrl.URL.projectFramworkCtrl.add,
      data: {projectStr: JSON.stringify(requestDate)},
      success: (res) => {
        if (res.success) {
          me._notification.success(`成功了`, res.info);
          defer.resolve(res.data);
        } else {
          me._notification.error(`出错了`, res.info);
        }
      },
      error: () => {
        me._notification.error(`出错了`, '失败，请稍后重试');
      }
    });
    return defer.promise();
  }

  /**
   * 关联技术框架
   * @param requestDate
   * @param callback
   */
  modifyFrames(requestDate: any) {
    let me = this, defer = $.Deferred(); //封装异步请求结果
    this.ajaxService.post({
      url: SettingUrl.URL.projectFramworkCtrl.add,
      data: {projectStr: JSON.stringify(requestDate)},
      success: (res) => {
        if (res.success) {
          me._notification.success(`成功了`, res.info);
          defer.resolve(res.data);
        } else {
          me._notification.error(`出错了`, res.info);
        }
      },
      error: () => {
        me._notification.error(`出错了`, '失败，请稍后重试');
      }
    });
    return defer.promise();
  }

  /**
   * 创建账户
   * @param requestDate
   * @param callback
   */
  buildRepository(requestDate: any) {
    let me = this, defer = $.Deferred(); //封装异步请求结果
    this.ajaxService.post({
      url: SettingUrl.URL.projectRepositoryAccountCtrl.build,
      data: requestDate,
      success: (res) => {
        if (res.success) {
          me._notification.success(`成功了`, res.info);
          defer.resolve(res.data);
        } else {
          me._notification.error(`出错了`, res.info);
        }
      },
      error: () => {
        me._notification.error(`出错了`, '失败，请稍后重试');
      }
    });
    return defer.promise();
  }

  /**
   * 修改账户
   * @param requestDate
   * @param callback
   */
  modifyRepository(requestDate: any) {
    let me = this, defer = $.Deferred(); //封装异步请求结果
    this.ajaxService.put({
      url: SettingUrl.URL.projectRepositoryAccountCtrl.modify,
      data: requestDate,
      success: (res) => {
        if (res.success) {
          me._notification.success(`成功了`, res.info);
          defer.resolve(res.data);
        } else {
          me._notification.error(`出错了`, res.info);
        }
      },
      error: () => {
        me._notification.error(`出错了`, '失败，请稍后重试');
      }
    });
    return defer.promise();
  }

  /**
   * load账户
   * @param requestDate
   * @param callback
   */
  loadRepository(requestDate: any) {
    let me = this, defer = $.Deferred(); //封装异步请求结果
    this.ajaxService.get({
      url: SettingUrl.URL.projectRepositoryAccountCtrl.load,
      data: requestDate,
      success: (res) => {
        if (res.success) {
          defer.resolve(res.data);
        } else {
          me._notification.error(`出错了`, res.info);
        }
      },
      error: () => {
        me._notification.error(`出错了`, '失败，请稍后重试');
      }
    });
    return defer.promise();
  }

  /**
   * 根据操作步骤跳到相应页面
   * @param current （当前步骤）
   */
  routerSkip(current, type) {
    switch (current) {
      case 0 :
        this.router.navigate([SettingUrl.ROUTERLINK.buildPro.proInfo], {'queryParams': {'type': type}});
        break;
      case 1 :
        this.router.navigate([SettingUrl.ROUTERLINK.buildPro.proSql], {'queryParams': {'type': type}});
        break;
      case 2 :
        this.router.navigate([SettingUrl.ROUTERLINK.buildPro.proDatabase], {'queryParams': {'type': type}});
        break;
      case 3 :
        this.router.navigate([SettingUrl.ROUTERLINK.buildPro.proFrames], {'queryParams': {'type': type}});
        break;
      case 4 :
        this.router.navigate([SettingUrl.ROUTERLINK.buildPro.proRepository], {'queryParams': {'type': type}});
        break;
    }
  }

  /**
   * 查询该项目的所有表
   */
  getTableList(requestDate: any) {
    let me = this; //返回异步请求结果
    return new Promise((resolve) => {
      this.ajaxService.get({
        url: SettingUrl.URL.mapRelationship.listMapClassTable,
        data: requestDate,
        success: (res) => {
          if (res.success) {
            resolve(res.data);
          } else {
            me._notification.error(`出错了`, res.info);
          }
        },
        error: () => {
          me._notification.error(`出错了`, '失败，请稍后重试');
        }
      });
    });
  }

  /**
   * 查询该项目的所有表关系
   */
  getTableRelationshipList(requestDate: any) {
    let me = this; //返回异步请求结果
    return new Promise((resolve) => {
      this.ajaxService.get({
        url: SettingUrl.URL.mapRelationship.relationshipList,
        data: requestDate,
        success: (res) => {
          if (res.success) {
            resolve(res.data);
          } else {
            me._notification.error(`出错了`, res.info);
          }
        },
        error: () => {
          me._notification.error(`出错了`, '失败，请稍后重试');
        }
      });
    });
  }

  /**
   * 创建表关系
   */
  buildRelationship(requestDate: any) {
    let me = this; //返回异步请求结果
    return new Promise((resolve) => {
      this.ajaxService.post({
        url: SettingUrl.URL.mapRelationship.build,
        data: requestDate,
        success: (res) => {
          if (res.success) {
            resolve(true);
            this.message.success('操作成功');
          } else {
            me._notification.error(`出错了`, res.info);
          }
        },
        error: () => {
          me._notification.error(`出错了`, '失败，请稍后重试');
        }
      });
    });
  }

  /**
   * 查询表的所有字段
   */
  getListMapFieldColumn(requestDate: any) {
    let me = this; //返回异步请求结果
    return new Promise((resolve) => {
      this.ajaxService.get({
        url: SettingUrl.URL.mapRelationship.listMapFieldColumn,
        data: requestDate,
        success: (res) => {
          if (res.success) {
            resolve(res.data);
          } else {
            me._notification.error(`出错了`, res.info);
          }
        },
        error: () => {
          me._notification.error(`出错了`, '失败，请稍后重试');
        }
      });
    });
  }

  /**
   * 删除表关系
   */
  deleteRelationship(requestDate: any) {
    let me = this; //返回异步请求结果
    return new Promise((resolve) => {
      this.ajaxService.del({
        url: SettingUrl.URL.mapRelationship.delete,
        data: requestDate,
        success: (res) => {
          if (res.success) {
            resolve(true);
            this.message.success('操作成功');
          } else {
            me._notification.error(`出错了`, res.info);
          }
        },
        error: () => {
          me._notification.error(`出错了`, '失败，请稍后重试');
        }
      });
    });
  }

  /**
   * 获取表字段及其属性
   */
  getFieldsList(requestDate: any) {
    let me = this; //返回异步请求结果
    return new Promise((resolve,reject) => {
      this.ajaxService.get({
        url: SettingUrl.URL.attributes.fieldsList,
        data: requestDate,
        success: (res) => {
          if (res.success) {
            resolve(res.data);
          } else {
            reject(false);
            me._notification.error(`出错了`, res.info);
          }
        },
        error: () => {
          reject(false);
          me._notification.error(`出错了`, '失败，请稍后重试');
        }
      });
    });
  }

  /**
   * 保存表字段属性
   */
  fieldsSave(requestDate: any) {
    let me = this; //返回异步请求结果
    return new Promise((resolve) => {
      this.ajaxService.post({
        url: SettingUrl.URL.attributes.fieldsSave,
        data: JSON.stringify(requestDate),
        contentType: 'application/json',
        success: (res) => {
          if (res.success) {
            resolve(true);
            this.message.success(res.info);
          } else {
            me._notification.error(`出错了`, res.info);
          }
        },
        error: () => {
          me._notification.error(`出错了`, '失败，请稍后重试');
        }
      });
    });
  }
}
