import {NgModule} from '@angular/core';
import {TemplateComponent} from './template/template.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";
import {TemplateService} from "./template.service";
import { TreeComponent } from './tree/tree.component';
import { SummaryComponent } from './summary/summary.component';

const routes: Routes = [
  {path: '', component: TemplateComponent},
  {path: 'summary', component: SummaryComponent},
];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(routes)
  ],
  providers: [TemplateService],
  declarations: [TemplateComponent, TreeComponent, SummaryComponent]
})
export class TemplateModule {
}
