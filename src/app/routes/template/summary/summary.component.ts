import { Component, OnInit } from '@angular/core';
import {Setting} from '../../../public/setting/setting';

@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.scss']
})
export class SummaryComponent implements OnInit {
  summary = Setting.summary;

  constructor() { }

  ngOnInit() {
  }

}
