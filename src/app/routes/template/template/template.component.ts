import {Component, OnInit} from '@angular/core';
// import { Terminal } from 'xterm';
// import { fit } from 'xterm/lib/addons/fit/fit';
// const xterm = new Terminal();


@Component({
  selector: 'app-template',
  templateUrl: './template.component.html',
  styleUrls: ['./template.component.css']
})
export class TemplateComponent implements OnInit {
  tabs = [];
  public editorOptions = {
    theme: 'vs-dark',
    language: 'text',
    scrollBeyondLastLine: false,
    readOnly: true,
  };
  curChecked: string;

  public project = {
    title: 'root1',
    key: '1001',
    expanded: true,
    type: 1,
    children: [
      {
        title: 'grandchild1.2.1',
        key: '1000121',
        isLeaf: true,
        type: 2,
      },
      {
        title: 'grandchild1.2.2',
        key: '1000122',
        isLeaf: true,
        type: 3,
      }
    ]
  }

  public project2 = {
    title: 'root1',
    key: '1001',
    expanded: true,
    children: [
      {
        title: 'child1',
        key: '10001',
        type: 1,
        children: [
          {
            title: 'child1.1',
            key: '100011',
            type: 1,
            children: []
          },
          {
            title: 'child1.2',
            key: '100012',
            type: 1,
            children: [
              {
                title: 'grandchild1.2.1',
                key: '1000121',
                isLeaf: true,
                type: 2,
              },
              {
                title: 'grandchild1.2.2',
                key: '1000122',
                isLeaf: true,
                type: 3,
              }
            ]
          }
        ]
      }
    ]
  }

  public selectedIndex: number = 0;

  constructor() {
  }

  ngOnInit() {
    // xterm.open(document.getElementById('terminal'));
    // xterm.write('Hello from \x1B[1;3;31mxterm.js\x1B[0m $ ');
    // fit(xterm);
  }

  closeTab(tab) {
    this.tabs.splice(this.tabs.indexOf(tab), 1);
  }

  /**
   * 获取当前选中的文件
   * @param data
   */
  getCurChecked(data) {
    console.log(data);
    this.curChecked = data.curChecked;
    if (!this.tabs.some(item => item.name === this.curChecked)) {
      this.tabs.push({
        name: this.curChecked,
        sourceCode: this.curChecked,
        type: 'text'
      });
    };
    this.selectedIndex = this.tabs.findIndex(item => item.name == this.curChecked)
  }

  /**
   * tab切换的时候
   * @param index
   */
  selectedIndexChange(index){
    this.editorOptions.language = this.tabs[index].type;
  }
}
