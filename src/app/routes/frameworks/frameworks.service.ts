import { Injectable } from '@angular/core';
import {AjaxService} from '../../public/service/ajax.service';
import {SettingUrl} from '../../public/setting/setting_url';
import {NzNotificationService} from "ng-zorro-antd";
import {Router} from "@angular/router";

import * as $ from "jquery";

@Injectable({
  providedIn: 'root'
})
export class FrameworksService {

  constructor(public router: Router, private ajaxService:AjaxService,
              public _notification: NzNotificationService) { }

  /**
   * 获取技术框架的数据
   * @param requestDate
   * @param callback
   */
  framesList(requestDate: any) {
    let me = this, defer = $.Deferred(); //封装异步请求结果
    this.ajaxService.get({
      url: SettingUrl.URL.frameworksCtrl.list,
      data: requestDate,
      success: (res) => {
        if (res.success) {
          defer.resolve(res.data);
        } else {
          me._notification.error(`出错了`, res.info)
        }
      },
      error: () => {
        me._notification.error(`出错了`, '失败，请稍后重试')
      }
    });
    return defer.promise();
  }

  /**
   * 新增技术框架
   * @param requestDate
   * @param callback
   */
  buildFrames(requestDate: any) {
    let me = this, defer = $.Deferred(); //封装异步请求结果
    this.ajaxService.post({
      url: SettingUrl.URL.frameworksCtrl.build,
      data: requestDate,
      success: (res) => {
        if (res.success) {
          defer.resolve(res.data);
        } else {
          me._notification.error(`出错了`, res.info)
        }
      },
      error: () => {
        me._notification.error(`出错了`, '失败，请稍后重试')
      }
    });
    return defer.promise();
  }


  /**
   * 查询技术框架详情
   * @param requestDate
   * @param callback
   */
  loadFrameworkss(requestDate: any) {
    let me = this, defer = $.Deferred(); //封装异步请求结果
    this.ajaxService.get({
      url: SettingUrl.URL.frameworksCtrl.load,
      data: requestDate,
      success: (res) => {
        if (res.success) {
          defer.resolve(res.data);
        } else {
          me._notification.error(`出错了`, res.info)
        }
      },
      error: () => {
        me._notification.error(`出错了`, '失败，请稍后重试')
      }
    });
    return defer.promise();
  }

  /**
   * 修好技术架构
   * @param requestDate
   * @param callback
   */
  modifyFrameworks(requestDate: any) {
    let me = this, defer = $.Deferred(); //封装异步请求结果
    this.ajaxService.post({
      url: SettingUrl.URL.frameworksCtrl.modify,
      data: requestDate,
      success: (res) => {
        if (res.success) {
          me._notification.success(`成功了`, res.info);
          defer.resolve(res.data);
        } else {
          me._notification.error(`出错了`, res.info);
          defer.reject(res.data);
        }
      },
      error: () => {
        me._notification.error(`出错了`, '失败，请稍后重试')
      }
    });
    return defer.promise();
  }

  /**
   * 修好技术架构
   * @param requestDate
   * @param callback
   */
  deleteFrameworks(requestDate: any) {
    let me = this, defer = $.Deferred(); //封装异步请求结果
    this.ajaxService.del({
      url: SettingUrl.URL.frameworksCtrl.delete,
      data: requestDate,
      success: (res) => {
        if (res.success) {
          me._notification.success(`成功了`, res.info);
          defer.resolve(true);
        } else {
          me._notification.error(`出错了`, res.info);
          defer.reject(false);
        }
      },
      error: () => {
        me._notification.error(`出错了`, '失败，请稍后重试')
      }
    });
    return defer.promise();
  }


  /**
   * 修好技术架构
   * @param requestDate
   * @param callback
   */
  modifyFrameworksState(requestDate: any) {
    let me = this, defer = $.Deferred(); //封装异步请求结果
    this.ajaxService.post({
      url: SettingUrl.URL.frameworksCtrl.modifyIsPublic,
      data: requestDate,
      success: (res) => {
        if (res.success) {
          me._notification.success(`成功了`, res.info);
          defer.resolve(true);
        } else {
          me._notification.error(`出错了`, res.info);
          defer.reject(false);
        }
      },
      error: () => {
        me._notification.error(`出错了`, '失败，请稍后重试')
      }
    });
    return defer.promise();
  }
}
