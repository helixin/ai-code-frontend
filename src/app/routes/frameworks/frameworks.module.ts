import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {FrameworksComponent} from './frameworks/frameworks.component';
import {SharedModule} from '../../shared/shared.module';
import {FrameworksService} from './frameworks.service';

const routes: Routes = [
  {path: '', component: FrameworksComponent},
];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(routes),
  ],
  declarations: [FrameworksComponent],
  providers: [FrameworksService]
})
export class FrameworksModule { }
