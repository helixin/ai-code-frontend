import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {Setting} from '../../public/setting/setting';
import {SettingUrl} from '../../public/setting/setting_url';
import {Router} from '@angular/router';

@Component({
  selector: 'app-simple',
  templateUrl: './simple.component.html',
  styleUrls: ['./simple.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class SimpleComponent implements OnInit {
  public app = Setting.APP; //平台信息
  public frameworks = SettingUrl.ROUTERLINK.frameworks.list;    //新建项目的路径
  public template = SettingUrl.ROUTERLINK.template;    //新建项目的路径
  public home = SettingUrl.ROUTERLINK.project.list;
  public summary = SettingUrl.ROUTERLINK.summary;

  constructor(public router: Router) {
  }

  ngOnInit() {
  }

  goAddTec() {
    this.router.navigate([this.frameworks]);
  }

  readSummary(){
    this.router.navigate([this.summary]);
  }

  backHome(){
    this.router.navigate([this.home]);
  }

  /**
   * 退出登录
   */
  logout() {
    sessionStorage.removeItem('token');
    this.router.navigate([SettingUrl.ROUTERLINK.login.login]); //路由跳转（登录页面）
  }
}
