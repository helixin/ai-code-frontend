/*基本属性配置*/

export class Setting {
  public static APP: any = {                           //平台信息
    name: 'AI-Code代码自动化生成',
    description: 'AI-Code代码自动生成',
    copyright: '© 2017 - AI-Code',
    logo: 'assets/img/logo-color.png',
    defaultImg: 'assets/img/dummy.png',
    userDefaultImg: 'assets/img/user-default.png'
  };
  public static PAGEMSG: any = {                        //平台信息提示（公式、提示、引导等等...）
    tipTitle: '操作提示',
    message: {
      proInfo: [
        '项目英文名将作为数据库名使用，请遵循命名规范避免失败',
        '版权文字将注释在文件头部',
        '项目基础包名范例：com.rzhkj的格式，结尾不要有“.点”出现',
        '项目描述仅描述生成任务内容，不会参与代码生成任务',
      ],
      proFrames: [
        '技术名称代表技术框架的组合，通常只需要选择一个组合就可以满足需要',
        '多个项目建议分开建立，或者以模块为单位进行建立',
      ],
      proDatabase: [
        '表模型之间的不能同时满足两种关系，修改关系时，请先取消原来的关系',
        '修改字段属性时请及时保存，切换表将不会保留当前表的修改',
      ],
      proSql: [
        '建议使用工具导出的sql脚本',
        '脚本应该经过测试可以正常创建数据库或者数据库全套表',
        '数据库脚本将默认采用mysql数据库进行连接，并用于代码解析与生成，请注意与mysql的兼容性问题',
      ],
      proRepository: [
        '账户密码是git或者是svn的账户密码，必须具备提交代码的权限',
        '仓库地址为完成的http或者https地址，如：<a href="https://gitee.com/helixin/AI-Code.git" target="_blank">https://gitee.com/helixin/AI-Code.git</a>',
      ],
      frameworks: [
        '技术名称代表技术框架的组合，以“+”链接'
      ]
    }
  };

  public static summary = '<p>映射模板的输出数据定义</p>' +
    '<p>需要新参数在此类中定义即可</p>' +
    '<p>模板采用freemarker</p>' +
    '<p>模板可以拿到如下数据：</p>' +
    '<p><span class="color-theme">${projectName}</span> <span class="color-blue">项目英文名</span></p>' +
    '<p><span class="color-theme">${model}</span> <span class="color-blue">模块中的模型 -> ${basePackage}.${model}.service</span></p>' +
    '<p><span class="color-theme">${module}</span> <span class="color-blue">模块 一个项目的模块化 不参与java的包定义只是项目管理分离办法</span></p>' +
    '<p><span class="color-theme">${basePackage}</span> <span class="color-blue">包名</span></p>' +
    '<p><span class="color-theme">${table}</span> <span class="color-blue">表对象</span></p>' +
    '<p><span class="color-theme">${clazz}</span> <span class="color-blue">类对象</span></p>' +
    '<p><span class="color-theme">${tableName}</span> <span class="color-blue">表名</span></p>' +
    '<p><span class="color-theme">${className}</span> <span class="color-blue">类名</span></p>' +
    '<p><span class="color-theme">${classNameLower}</span> <span class="color-blue">类名小写</span></p>' +
    '<p><span class="color-theme">${notes}</span> <span class="color-blue">类注释</span></p>' +
    '<p><span class="color-theme">${copyright}</span> <span class="color-blue">项目版权</span></p>' +
    '<p><span class="color-theme">${author}</span> <span class="color-blue">作者</span></p>' +
    '<p><span class="color-theme">${classes}</span> <span class="color-blue">类信息对象 集合</span></p>' +
    '<p><span class="color-theme">${columns}</span> <span class="color-blue">列对象 集合</span></p>' +
    '<p><span class="color-theme">${pkColumns}</span> <span class="color-blue">主键数据信息</span></p>' +
    '<p><span class="color-theme">${notPkColumns}</span> <span class="color-blue">非主键数据信息</span></p>' +
    '<p><span class="color-theme">${fields}</span> <span class="color-blue">类属性 集合</span></p>' +
    '<p><span class="color-theme">${pkFields}</span> <span class="color-blue">主键数据信息</span></p>' +
    '<p><span class="color-theme">${notPkFields}</span> <span class="color-blue">非主键主键数据信息</span></p>' +
    '<p><span class="color-theme">${modelClasses}</span> <span class="color-blue">各个模块下的所有类集合信息</span></p>' +
    '<p><span class="color-theme">${relationships}</span> <span class="color-blue">类与类之间的关联关系模型集合信息</span></p>' +
    '<p><span class="color-theme">${displayAttributes}</span> <span class="color-blue">所有类下面的属性集合信息</span></p>' +
    '<p><span class="color-theme">${tableFields}</span> <span class="color-blue">前端页面显示</span></p>' +
    '<p><span class="color-theme">${modelDatas}</span> <span class="color-blue">模型与实体类的关系</span></p>' +
    '<p><span class="color-theme">${dashedCaseName}</span> <span class="color-blue">破折号命名 或叫烤串命名 适用于 前端angular ,react, vue</span></p>';

  public static MENUS: Array<any> = [];      //平台菜单
  //定义枚举
  static ENUM: any = {
    articleState: 1005,  //eg文章状态枚举
  };
}
