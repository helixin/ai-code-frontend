/*接口访问路径配置*/

const host = location.host;

export class SettingUrl {
  // 接口通讯url集合
  static URL: any = {
    /**
     * 基础路径配置
     */
    base: {
      enum: '/res/enum/',            //获取枚举接口
      uuid: '/upload/basic/uid',    //获取上传图片的编码
      websocket: `ws://${host}/websocket.shtml` //实时获取日志 TODO 地址是本地的
    },
    /**
     * 项目管理控制器
     */
    projectCtrl: {
      build: '/project/build',//（post）创建项目
      delete: '/project/delete',//（delete）删除项目
      init: '/project/init',//（put）执行脚本
      list: '/project/list',//（get）查询项目信息集合 按照时间顺序倒叙排序
      load: '/project/load',//（get）查询一个详情信息
      modify: '/project/modify',//（put）修改项目
      path: '/project/scan/path',//（get）查询文件路径
    },
    /**
     * 项目任务管理控制器
     */
    projectJobCtrl: {
      build: '/project/job/build',//（post）创建任务
      execute: '/project/job/execute',//（get）执行任务
      delete: '/project/job/delete',//（delete）删除任务
      list: '/project/job/list',//（get）查询任务列表
      load: '/project/job/load',//（get）load当前任务
      modify: '/project/job/modify',//（put）修改任务
    },
    /**
     * 项目日志管理控制器
     */
    projectJobLogsCtrl: {
      logs: '/logs/load',//（get）查询项目任务信息集合
    },
    /**
     * 框架技术控制器
     */
    frameworksCtrl: {
      build: '/framework/build',//（post）添加
      delete: '/framework/delete',//（delete）删除
      list: '/framework/list',//（get）查询信息集合 按照时间顺序倒叙排序
      load: '/framework/load',//（get）查询一个详情信息
      modify: '/framework/modify',//（post）修改
      modifyIsPublic: '/framework/updateIsPublic',//（post）修改是否公开
    },
    /**
     * 项目框架技术关联控制器
     */
    projectFramworkCtrl: {
      add: '/project/framwork/add',//（post）添加项目技术
      delete: '/project/framwork/delete',//（delete）删除
      list: '/project/framwork/list',//（get）查询信息集合 按照时间顺序倒叙排序
      load: '/project/framwork/load',//（get）查询一个详情信息
    },
    /**
     * 代码仓库账户管理控制器
     */
    projectRepositoryAccountCtrl: {
      build: '/project/repository/build',//（post）创建账户
      delete: '/project/repository/delete',//（delete）删除
      list: '/project/repository/list',//（get）查询信息集合 按照时间顺序倒叙排序
      load: '/project/repository/load',//（get）查询一个详情信息
      modify: '/project/repository/modify',//（put）修改
    },
    /**
     * 项目sql管理控制器
     */
    projectSqlCtrl: {
      build: '/project/sql/build',//（post）创建项目sql
      delete: '/project/sql/delete',//（delete）删除项目sql
      list: '/project/sql/list',//（get）查询sql信息集合 按照时间顺序倒叙排序
      load: '/project/sql/load',//（get）查询一个sql详情信息
      modify: '/project/sql/modify',//（put）修改sql
    },
    mapRelationship: {
      listMapClassTable: '/project/relationship/listMapClassTable', //数据表
      listByProjectCode: '/project/relationship/listByProjectCode', //关系数据表
      relationshipList: '/project/relationship/list', //关系数据表
      listMapFieldColumn: '/project/relationship/listMapFieldColumn', //数据表所有字段
      build: '/project/relationship/build', //创建关系
      delete: '/project/relationship/delete', //删除关系
    },
    attributes: {
      fieldsList: '/displayAttribute/list',//表字段及其属性
      fieldsSave: '/displayAttribute/save',//保存字段属性
    },

    /**
     * 登录注册
     */
    login: {
      signin: '/login/signin',//登录
      reg: '/login/reg'//注册
    }
  };
  // 路由链接信息
  static ROUTERLINK: any = {
    buildPro: {
      proInfo: '/main/buildPro/proSteps/proInfo', //填写项目基本信息页面
      proFrames: '/main/buildPro/proSteps/proFrames', //填写项目所选技术框架面
      proRepository: '/main/buildPro/proSteps/proRepository', //填写项目仓库页面
      proDatabase: '/main/buildPro/proSteps/proDatabase', //数据管理
      proSql: '/main/buildPro/proSteps/proSql', //填写项目sql页面
    },
    frameworks: {
      list: '/main/frameworks', //管理技术架构
    },
    project: {
      list: '/main/project',//项目的列表
      detail: '/main/project/detail',//项目的详情
      logs: '/main/project/logs',//项目的详情
    },
    login: {
      login: '/login/index', //登录
      reg: '/login/index/reg' //注册
    },
    template: '/main/template',
    summary: '/main/template/summary'
  };
}
