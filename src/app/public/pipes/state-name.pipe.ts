import {Pipe, PipeTransform} from '@angular/core';
import {MainService} from '../service/main.service';

@Pipe({
  name: 'stateName'
})
export class StateNamePipe implements PipeTransform {

  constructor(private mainService: MainService) {

  }

  transform(value: any, args?: any): any {
    let me = this, val;
    val = me.mainService.getEnumDataValByKey(args, value);
    return val;
  }

}
